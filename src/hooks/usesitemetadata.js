import { graphql, useStaticQuery } from "gatsby";

const useSiteMetadata = () => {
    const data = useStaticQuery(graphql`
    query SiteTitleQuery {
        site {
        siteMetadata {
            title
        }
        }
    }
    `)
    return data;
};

export { useSiteMetadata };
