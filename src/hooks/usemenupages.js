import { useStaticQuery, graphql } from "gatsby"

export const useMenuPages = languageCode => {
  /*
    Usage:
    const pages = useMenuPages("en");
    this will return all pages that are direct descendants of the page with slug "en".
    Same goes for the slug "nl".
    */
  const { wagtail } = useStaticQuery(
    graphql`
      query MenuPages {
        wagtail {
          nl: page(slug: "nl") {
            children {
              title
              url
              showInMenus
              id
            }
          }
          en: page(slug: "en") {
            children {
              title
              url
              showInMenus
              id
            }
          }
        }
      }
    `
  )
  const lang = wagtail[languageCode]
  const pages = lang?.children ?? []
  return pages.filter(page => page.showInMenus)
}
